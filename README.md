# `@ctrlc/vite-plugin-vue-setup-extend`

> 用于`vue3`在`<script setup>`语法时，扩展`name`以及`inheritAttrs`属性, 用于显式声明组件展示时的名称、用于控制是否启用默认的组件`attribute`透传行为

## 使用手册

### 安装

```bash
npm i @ctrlc/vite-plugin-vue-setup-extend -D
```

### 语法

```JS
vueScriptExtend(Options)
```

### 配置项

参数|类型|默认值|说明
----|---|-------|---
Options.include| FilterPattern | undefined | rollup过滤模式
Options.exclude| FilterPattern | undefined | rollup过滤模式
Options.hasMap| boolean | true | 是否启用 magic-string map方法 优化控制台日志
Options.hires| boolean | true | magic-string 配置参数

### 示例

在`vite.config.ts`文件中引入插件：

```js
...
import vue from '@vitejs/plugin-vue'
import vueScriptExtend from '@ctrlc/vite-plugin-vue-setup-extend';

export default defineConfig({
  plugins: [vue(), vueScriptExtend()],
})
```

```html
<template>
  <div>hello world</div>
</template>
<!-- 扩展 setup模式 script属性 -->
<script lang="ts" setup name="App" inherit-attrs="false">
  // code here
</script>
```

编译后转义为

```js
import { defineComponent } from 'vue'

export default defineComponent({
  name: "App",
  inheritAttrs: false,
})

```

## 文档

* [文档](https://gitee.com/ctrlc-git/ctrlc-vite-setup-extend.git)
