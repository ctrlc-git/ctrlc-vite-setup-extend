import type { Plugin, FilterPattern } from 'vite';
import { createFilter } from 'vite';
import { transformScript } from './lib';

export interface Options {
  include?: FilterPattern | undefined;
  exclude?: FilterPattern | undefined;
  hasMap?: boolean;
  hires?: boolean;
}

// viteScriptName
export default (options: Options = {}): Plugin => {
  const filter = createFilter(options.include, options.exclude);
  return {
    name: 'vite:ctrlc-setup-extend',
    enforce: 'pre',
    transform(code: string, id: string) {
      if (!filter(id)) return;
      if (!/\.vue$/.test(id)) return;
      return transformScript({ code, id }, options);
    },
  };
};
