import { babel } from '@rollup/plugin-babel';
import commonjs from '@rollup/plugin-commonjs';
import { nodeResolve } from '@rollup/plugin-node-resolve';
import typescript from '@rollup/plugin-typescript';
import terser from '@rollup/plugin-terser';

export default {
  input: 'src/index.ts',
  output: [
    {
      file: 'lib/index.mjs',
      format: 'esm',
      exports: 'default',
      plugins: [terser()],
    },
    {
      file: 'lib/index.cjs',
      format: 'cjs',
      exports: 'default',
      plugins: [terser()],
    },
  ],
  external: ['vite', 'vue', 'vue/compiler-sfc', 'magic-string'],
  plugins: [
    nodeResolve(),
    typescript({ tsconfig: './tsconfig.json', include: ['src/*'] }),
    commonjs(),
    babel({ babelHelpers: 'bundled' }),
  ],
};
